podman build -t performance-tracking .

podman run -it --rm                             \
    --name performance-tracking                 \
    --env-file env.txt                          \
    -v ~/.gitlab-token:/.gitlab-token           \
    -v ~/.influx-token:/.influx-token           \
    -v ~/panfrost-prefix/source/mesa-test:/mesa \
    performance-tracking

The mesa repo needs to have these two remotes:

[tomeu@cizrna mesa-test]$ git remote -v
origin	https://gitlab.freedesktop.org/tomeu/mesa.git (fetch)
origin	https://gitlab.freedesktop.org/tomeu/mesa.git (push)
upstream	https://gitlab.freedesktop.org/mesa/mesa.git (fetch)
upstream	https://gitlab.freedesktop.org/mesa/mesa.git (push)

[tomeu@cizrna mesa-scripts]$ cat env.txt
INFLUX_URL=http://10.42.0.1:8086/
INFLUX_ORG=mesa
INFLUX_BUCKET=mesa
GITLAB_PROJECT_PATH=tomeu/mesa

