FROM alpine:latest

RUN apk add python3 py3-pip bash git && \
pip3 install --upgrade pip && \
pip3 install requests && \
pip3 install python-gitlab && \
pip3 install influxdb-client && \
rm -rf /var/cache/apk/*

COPY trigger-jobs.py .
COPY upload-data.py .
COPY update-mesa.sh .

CMD ["/update-mesa.sh"]
