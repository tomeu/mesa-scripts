#!/bin/bash

set -x

GITLAB_TOKEN=`cat /.gitlab-token`

git config --global user.email "tomeu.vizoso@collabora.com"
git config --global user.name "Tomeu Vizoso"

cd /mesa
git rebase --abort # Just in case
git remote update origin
git checkout ci-performance-tracking
git reset --hard origin/ci-performance-tracking
git pull --rebase upstream main
git push --force https://tomeu:$GITLAB_TOKEN@gitlab.freedesktop.org/tomeu/mesa.git ci-performance-tracking:ci-performance-tracking

/trigger-jobs.py $(git rev-parse HEAD)

/upload-data.py $(git rev-parse HEAD)

