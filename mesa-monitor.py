#!/usr/bin/python3

# Copyright (c) 2020 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

# Tool to trigger a subset of container jobs and monitor the progress of a test job
#
# Usage: mesa-monitor.py $(git rev-parse HEAD) target-job (eg. arm64_a630_traces) dependencies (eg. arm_build arm_test-base)

import gitlab
import os
import sys
import time

COMPLETED_STATUSES = ['success', 'failed']

def get_mesa_project(gl):
    for project in gl.projects.list(owned=True):
       if project.name == 'mesa':
            return project

def wait_for_pipeline(project, sha):
    while True:
        pipelines = project.pipelines.list(sha=sha)
        if pipelines:
            return pipelines[0]
        time.sleep(1)

def monitor_pipeline(project, pipeline, target_job, dependencies):
    statuses = {}
    while True:
        for job in pipeline.jobs.list(all=True):
            if job.name == target_job:
                if job.status in ['running']:
                    print('Target job %s started' % job.name)
                    return job
                elif job.status in ['success', 'failed']:
                    print('Target job %s is complete' % job.name)
                    return job

            if job.id in statuses.keys():
                if statuses[job.id] not in COMPLETED_STATUSES and \
                    job.status in COMPLETED_STATUSES:
                    print('Job %s changed status to %s' % (job.name, job.status))
            statuses[job.id] = job.status

            if job.name in dependencies and job.status == 'manual':
                pjob = project.jobs.get(job.id, lazy=True)
                pjob.play()
                print('Triggered dependency %s' % job.name)

        time.sleep(5)

def print_logs(project, job_id):
    printed_lines = 0
    while True:
        job = project.jobs.get(job_id)

        # GitLab's REST API doesn't offer pagination for logs, so we have to refetch it all
        lines = job.trace().decode('unicode_escape').splitlines()
        for line in lines[printed_lines:]:
            print(line)        
        printed_lines = len(lines)

        if job.status in COMPLETED_STATUSES:
            return
        time.sleep(5)

gl = gitlab.Gitlab(url='https://gitlab.freedesktop.org',
                   private_token=open(os.path.expanduser('~/.gitlab-token')).read())

project = get_mesa_project(gl)
pipeline = wait_for_pipeline(project, sys.argv[1])
print('Pipeline: %s' % pipeline.web_url)
target_job = monitor_pipeline(project, pipeline, sys.argv[2], sys.argv[2:])
print_logs(project, target_job.id)
