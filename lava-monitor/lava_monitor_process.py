import csv

JOB_URL = 0
DATE = 1
HEALTH = 2
DEVICE = 3
DISPATCHER = 4
CAUSE = 5
DETAIL = 6

file = open('results.csv')
csvreader = csv.reader(file, delimiter=';')

def add(dict_, key, is_pass):
    if key not in dict_:
        dict_[key] = [0, 0]
    if is_pass:
        dict_[key][0] += 1
    else:
        dict_[key][1] += 1

def print_dict(dict_, total, name):
    print("{:<80} {:>11} {:>9} {:>15} %".format(name, "Total jobs", "Failures", "Failure rate"))
    dict_ = dict(sorted(dict_.items(), key=lambda x: x[0].lower()))
    for key in dict_.keys():
        if "Pass" in key:
            continue
        if total == 0:
            row_total = dict_[key][0] + dict_[key][1]
        else:
            row_total = total
        if dict_[key][1] > 0:
            print("{:<80} {:>11} {:>9} {:>15.2f} %".format(key[:80], dict_[key][0] + dict_[key][1], dict_[key][1], (dict_[key][1] / row_total) * 100))
    print()

error_sum = {}
device_sum = {}
dispatcher_sum = {}
dispatcher_x_error_sum = {}
device_x_error_sum = {}
total_jobs = 0
total_failures = 0
for line in csvreader:
    if line[CAUSE] == "Fail":
        continue
    total_jobs += 1
    if line[CAUSE] != "Pass":
        total_failures += 1
    add(error_sum, "%s: %s" % (line[CAUSE], line[DETAIL]), line[CAUSE] == "Pass")
    add(device_sum, line[DEVICE], line[CAUSE] == "Pass")
    add(dispatcher_sum, line[DISPATCHER], line[CAUSE] == "Pass")
    add(dispatcher_x_error_sum, "{} {} {}".format(line[DISPATCHER], line[CAUSE], line[DETAIL]), line[CAUSE] == "Pass")
    add(device_x_error_sum, "{} {} {}".format(line[DEVICE], line[CAUSE], line[DETAIL]), line[CAUSE] == "Pass")

print_dict(error_sum, total_jobs, "Error")
print_dict(device_sum, 0, "Device")
print_dict(dispatcher_sum, 0, "Dispatcher")
print_dict(dispatcher_x_error_sum, total_jobs, "Dispatcher+Error")
print_dict(device_x_error_sum, total_jobs, "Device+Error")

print("Total {}".format(total_jobs))
print("Failure rate {:.2f} %".format(total_failures / total_jobs * 100))