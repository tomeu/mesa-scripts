import xmlrpc.client
import json
import os

'''
import requests
import logging
import http.client
http.client.HTTPConnection.debuglevel = 1
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
requests_log = logging.getLogger("requests.packages.urllib3")
requests_log.setLevel(logging.DEBUG)
requests_log.propagate = True
'''

username = "tomeu"
token = open(os.path.expanduser('~/.lava-token')).read()
hostname = "lava.collabora.co.uk"
server = xmlrpc.client.ServerProxy("https://%s:%s@%s/RPC2" % (username, token, hostname), allow_none=True)

device_workers = {}

start = 0
page = 100
days = 1
since = days * 60 * 48
devices = {}
while True:
    jobs = server.scheduler.jobs.list("FINISHED", None, start, page, since, True)
    if not jobs:
        break;
    start += page
    for job in jobs:

        if job["actual_device"] not in device_workers:
            device_workers["actual_device"] = server.scheduler.devices.show(job["actual_device"])["worker"]

        _, log = server.scheduler.jobs.logs(job["id"])
        cause = "Unknown"
        detail = ""
        parsed_line = ""
        for line in str(log).splitlines():
            if "invoked oom-killer" in line:
                cause = "OOM"
                break
            if "nfs: server" in line:
                cause = "NFS"
                break
            elif "R8152: Bulk read error" in line:
                cause = "R8152"
                break
            elif "tftp-deploy timed out" in line:
                cause = "TFTP timeout"
                break
            elif "Connection was closed before we received a valid response from endpoint URL" in line:
                cause = "MinIO timeout"
                break
            elif job["health"] == "Canceled" and "Marking unfinished test run as failed" in line:
                parsed_line = json.loads(prev_line[2:])["msg"]
                if "GPU HANG" in parsed_line or \
                   "rcs0 reset request" in parsed_line:
                    detail = "GPU hang"
                elif "HTTP request sent," in parsed_line or \
                     "....." in parsed_line or \
                     "0K" in parsed_line or \
                     "Accept-Ranges" in parsed_line or \
                     "wget" in parsed_line or \
                     "caching-proxy" in parsed_line:
                    detail = "Network timeout"
                elif "Pass:" in parsed_line or \
                     "pass:" in parsed_line or \
                     "deqp-runner" in parsed_line or \
                     "dEQP error" in parsed_line or \
                     "./piglit run" in parsed_line:
                    detail = "Test suite"
                elif "kworker/dying" in parsed_line:
                    detail = "kworker/dying"
                else:
                    detail = parsed_line
                cause = "Output timeout"
                break
            elif job["health"] == "Canceled" and "The job was canceled" in line:
                parsed_line = json.loads(prev_line[2:])["msg"]
                if "[Enter `^Ec?' for help]" in parsed_line:
                    detail = "Depthcharge hang"
                else:
                    detail = parsed_line
                cause = "Output timeout"
                break
            elif "mesa: fail" in line:
                cause = "Fail"
                break
            elif "mesa: pass" in line:
                cause = "Pass"
                break
            prev_line = line

        print("https://lava.collabora.co.uk/scheduler/job/{};{};{};{};{};{};{};{}".format(job["id"], job["start_time"], job["health"], job["actual_device"], device_workers["actual_device"], cause, detail, parsed_line))
