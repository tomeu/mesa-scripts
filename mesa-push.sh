#!/bin/bash

SCRIPT=$0
TARGET_JOB=$1
shift
DEPS=$@

SCRIPT_DIR=$(dirname "$(readlink -f "$SCRIPT")")

git push --force tomeu $(git rev-parse --abbrev-ref HEAD)

$SCRIPT_DIR/mesa-monitor.py $(git rev-parse HEAD) $TARGET_JOB $DEPS

